package greencopper.addel.test.data;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import greencopper.addel.test.App;

public class Prefs {

    public void save(String serializedObjectKey, Object object) {
        SharedPreferences sharedPreferences = App.getInstance().getSharedPreferences(App.getInstance().getPackageName(), 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(object);
        sharedPreferencesEditor.putString(serializedObjectKey, serializedObject);
        sharedPreferencesEditor.apply();
    }


    public <GenericClass> GenericClass get(String preferenceKey, Class<GenericClass> classType) {
        SharedPreferences sharedPreferences = App.getInstance().getSharedPreferences(App.getInstance().getPackageName(), 0);
        if (sharedPreferences.contains(preferenceKey)) {
            final Gson gson = new Gson();
            return gson.fromJson(sharedPreferences.getString(preferenceKey, ""), classType);
        }
        return null;
    }
}
