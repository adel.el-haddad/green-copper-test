package greencopper.addel.test.di;


import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.IOException;

import dagger.Module;
import dagger.Provides;
import greencopper.addel.test.AppConstant;
import greencopper.addel.test.network.SpotifyService;
import greencopper.addel.test.data.Prefs;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {


    public ApiModule() {
    }

    @Provides
    SpotifyService provideSpotifyService(Retrofit retroFit) {
        return retroFit.create(SpotifyService.class);
    }

    @Provides
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(AppConstant.END_POINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    OkHttpClient provideOkHttpClient(Prefs prefs) {


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {

                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request.Builder requestBuilder = chain.request().newBuilder();
                        requestBuilder.removeHeader("Content-Type");
                        requestBuilder.addHeader("Content-Type", "application/json");
                        requestBuilder.addHeader("Authorization", "Bearer " +prefs.get("token",String.class));
                        return chain.proceed(requestBuilder.build());
                    }
                })
                .build();
    }

    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }
}
