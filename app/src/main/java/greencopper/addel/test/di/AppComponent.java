package greencopper.addel.test.di;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules={AppModule.class,ApiModule.class,ExoModule.class})
public interface AppComponent extends AppInjector{

}
