package greencopper.addel.test.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import greencopper.addel.test.data.Prefs;

@Module
public class AppModule {

    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }


    @Singleton
    @Provides
    Prefs providePrefs() {
        return new Prefs();
    }



}
