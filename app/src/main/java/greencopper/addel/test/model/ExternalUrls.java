
package greencopper.addel.test.model;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class ExternalUrls {

    @Expose
    private String spotify;

    public String getSpotify() {
        return spotify;
    }

    public void setSpotify(String spotify) {
        this.spotify = spotify;
    }

}
