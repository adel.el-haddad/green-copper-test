package greencopper.addel.test.network;

import com.google.gson.JsonObject;

import greencopper.addel.test.model.UserProfil;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SpotifyService {




    @GET("https://api.spotify.com/v1/playlists/{id}/tracks")
    Observable<JsonObject> getByPlayList(@Path(value = "id", encoded = true) String id);


    @GET("https://api.spotify.com/v1/me/top/tracks")
    Observable<JsonObject> getTopTracks();



    @GET("https://api.spotify.com/v1/me/")
    Observable<UserProfil> getUserProfil();




}
