package greencopper.addel.test.ui.auth;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import javax.inject.Inject;

import butterknife.OnClick;
import greencopper.addel.test.App;
import greencopper.addel.test.R;
import greencopper.addel.test.data.Prefs;
import greencopper.addel.test.ui.common.BaseActivity;
import greencopper.addel.test.ui.main.MainAct;


public class AuthActivity extends BaseActivity {


    @OnClick(R.id.tvLogin)
    void onClickLogin() {

        if (!spotifyInstalled()) {
            AuthenticationClient.openDownloadSpotifyActivity(this);
            quitWithMessage(getString(R.string.spotify_install_ap));
        } else {
            openAuthenticationActivity();
        }

    }


    @Inject
    Prefs prefs;


    private static final String TAG = "AuthActivity";
    private static final String CLIENT_ID = "e4ff8cdff1f248d186d596214a5dc296";
    private static final String REDIRECT_URI = "greencopper.addel.test://logincallback";
    private static final int REQUEST_CODE = 1337;


    @Override
    public void setDataToView(Bundle var1) {
        App.getInjector().inject(this);

    }

    @Override
    public int getLayout() {
        return R.layout.ui_act_login;
    }


    private void openAuthenticationActivity() {

        AuthenticationRequest request = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI)
                .setScopes(new String[]{"user-read-private",
                        "playlist-read-private",
                        "playlist-read-collaborative", "user-top-read"})
                .build();

        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
    }

    private void authenticationFailed(String reason) {
        setResult(RESULT_CANCELED);
        quitWithMessage(getString(R.string.spotify_auth_error) + " (" + reason + ")");
    }

    private void authenticationCancelled() {
        setResult(RESULT_CANCELED);
        quitWithMessage(getString(R.string.spotify_auth_required));
    }

    private boolean spotifyInstalled() {
        try {
            this.getPackageManager().getPackageInfo("com.spotify.music", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void quitWithMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            switch (response.getType()) {
                case TOKEN:
                    String token = response.getAccessToken();
                    int expiresInSeconds = response.getExpiresIn();


                    Log.d(TAG, "onActivityResult: " + token);
                    prefs.save("token", token);
                    prefs.save("expiresInSeconds", expiresInSeconds);


                    setResult(RESULT_OK);
                    startActivity(new Intent(AuthActivity.this, MainAct.class));
                    finish();
                    overridePendingTransition(R.anim.fade_in_hard_nodelay, R.anim.hide_delayed);
                    break;

                case ERROR:

                    authenticationFailed(response.getError());
                    break;
                default:

                    authenticationCancelled();
            }
        }
    }

}