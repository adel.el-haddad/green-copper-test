package greencopper.addel.test.ui.common;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.annotation.Nullable;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {
    @Nullable


    public BaseFragment() {
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        return inflater.inflate(this.getFragmentLayout(), container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.injectViews(view);

    }

    protected abstract int getFragmentLayout();

    private void injectViews(View view) {
        ButterKnife.bind(this, view);
    }
}
