package greencopper.addel.test.ui.common;

public interface BasePresenter {
    void subscribe();

    void unSubscribe();
}
