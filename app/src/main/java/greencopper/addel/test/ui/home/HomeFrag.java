package greencopper.addel.test.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import greencopper.addel.test.R;
import greencopper.addel.test.model.ViewEvent;
import greencopper.addel.test.ui.common.BaseFragment;
import greencopper.addel.test.view.AvenirViewPager;


public class HomeFrag extends BaseFragment {

    public static final String TAG = HomeFrag.class.getSimpleName();
    private HomePagerAdapter homePagerAdapter;

    @BindView(R.id.vpHome)
    AvenirViewPager vpHome;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        homePagerAdapter = new HomePagerAdapter(getChildFragmentManager());

        vpHome.setAdapter(homePagerAdapter);
        vpHome.setSwipeable(false);
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ViewEvent viewEvent) {
        if (viewEvent.getaClass() == HomeFrag.class) {
            if (viewEvent.getOperation().equals("next"))
                swipeRight(vpHome.getCurrentItem());
            else
                swipeLeft(vpHome.getCurrentItem());
        }
    }

    public void swipeRight(int x) {
        if (x < HomePagerAdapter.NUM_ITEMS) {
            vpHome.setCurrentItem(x + 1);
        }
    }

    public void swipeLeft(int x) {
        if (x > 0) {
            vpHome.setCurrentItem(x - 1);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        Log.d(TAG, "onPause: ");
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        Log.d(TAG, "onResume: ");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        Log.d(TAG, "onDestroy: ");

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.ui_frag_home;
    }


}
