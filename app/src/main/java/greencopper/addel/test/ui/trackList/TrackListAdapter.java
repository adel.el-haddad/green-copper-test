package greencopper.addel.test.ui.trackList;


import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import greencopper.addel.test.R;
import greencopper.addel.test.model.Image;
import greencopper.addel.test.model.Track;
import greencopper.addel.test.model.ViewEvent;
import greencopper.addel.test.ui.home.HomeFrag;
import greencopper.addel.test.utils.StringUtils;
import greencopper.addel.test.utils.TimeUtils;
import greencopper.addel.test.view.AvenirTextView;

public class TrackListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TRACK_ITEM = 0, TEXT_ITEM = 1;
    public ArrayList<Object> objects;
    private String TAG = getClass().getSimpleName();
    private Animation fadeIn;
    private Context con;
    private Activity act;
    private TrackListContract.ViewContract viewContract;
    private int audioPlayedPosition = -1;
    private boolean audioPlay = true;

    public TrackListAdapter(Activity act, TrackListContract.ViewContract viewContract, ArrayList<Object> objects) {
        this.objects = objects;
        this.act = act;
        this.con = act.getBaseContext();
        this.viewContract = viewContract;

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {

            case TEXT_ITEM:
                viewHolder = new TextHolder(inflater.inflate(R.layout.ui_row_text, parent, false));
                break;

            case TRACK_ITEM:
                viewHolder = new TrackHolder(inflater.inflate(R.layout.ui_row_track_2, parent, false));
                break;


            default:

                viewHolder = new TrackHolder(inflater.inflate(R.layout.ui_row_text, parent, false));

                break;

        }
        return viewHolder;


    }


    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof Track) {

            return TRACK_ITEM;
        } else if (objects.get(position) instanceof String) {

            return TEXT_ITEM;
        } else
            return -1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {


            case TRACK_ITEM:
                configureTrackItem((TrackHolder) viewHolder, position);
                break;
            case TEXT_ITEM:
                configureText((TextHolder) viewHolder, position);
                break;

        }


    }


    @Override
    public int getItemCount() {
        return objects.size();
    }


    private void configureText(TextHolder holder, int position) {

        holder.text.setText((String) objects.get(position));

        final ViewGroup.LayoutParams lp = holder.itemView.getLayoutParams();
        if (lp instanceof StaggeredGridLayoutManager.LayoutParams) {
            StaggeredGridLayoutManager.LayoutParams sglp = (StaggeredGridLayoutManager.LayoutParams) lp;
            sglp.setFullSpan(true);
            holder.itemView.setLayoutParams(sglp);
        }


    }


    private void configureTrackItem(final TrackHolder holder, final int position) {
        final Track track = (Track) objects.get(position);


        if (StringUtils.isPresent(track.getName()))
            holder.tvTrackTitle.setText(track.getName());


        if (track.getDurationMs() != null)
            holder.tvTrackDuration.setText(TimeUtils.getTime(track.getDurationMs()));

        holder.tvTrackDescription.setText((StringUtils.isPresent(track.getArtists().get(0).getName()) ? track.getArtists().get(0).getName() : " ") + " · " + (StringUtils.isPresent(track.getAlbum().getName()) ? track.getAlbum().getName() : ""));


        holder.tvTrackDescription.setText(track.getArtists().get(0).getName() + " · " + track.getAlbum().getName());

        if (track.getAlbum().getImages().size() > 0) {
            Image image = track.getAlbum().getImages().get(0);
            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(image.getUrl()))
                    .setResizeOptions(new ResizeOptions(450, 450))
                    .build();
            holder.sdvImageTrack.setController(
                    Fresco.newDraweeControllerBuilder()
                            .setOldController(holder.sdvImageTrack.getController())
                            .setImageRequest(request)
                            .build());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (track.getPreviewUrl() == null) {

                    Toast.makeText(con, "Error", Toast.LENGTH_LONG).show();
                } else {
                    viewContract.playOrPause(track);
                    EventBus.getDefault().post(new ViewEvent(HomeFrag.class, "next"));
                }

            }
        });


    }

    private void setSelected(int selected) {
        audioPlayedPosition = selected;
        TrackListAdapter.this.notifyDataSetChanged();

    }


    public class TrackHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tvTrackTitle)
        AvenirTextView tvTrackTitle;

        @BindView(R.id.tvTrackDescription)
        AvenirTextView tvTrackDescription;


        @BindView(R.id.tvTrackDuration)
        AvenirTextView tvTrackDuration;

        @BindView(R.id.audioContainer)
        LinearLayout audioContainer;

        @BindView(R.id.sdvImageTrack)
        SimpleDraweeView sdvImageTrack;

        public TrackHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public class TextHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text)
        AvenirTextView text;

        public TextHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}