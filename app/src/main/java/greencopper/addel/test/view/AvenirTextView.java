package greencopper.addel.test.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;


;import greencopper.addel.test.App;


public class AvenirTextView extends AppCompatTextView {

    private String TAG = "AvenirTextView", xmlFont;

    public AvenirTextView(Context context) {
        super(context);
        init();
    }

    public AvenirTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        String packageName = "http://schemas.android.com/apk/res-auto";
        try {
            xmlFont = attrs.getAttributeValue(packageName, "fontAvenir");

        } catch (Exception e) {

        }

        init();
    }

    public AvenirTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {

        if (isInEditMode()) {
            return;
        }

        if (xmlFont != null)
            setTypeface(App.getInstance().getTypeface(xmlFont));
        else
            setTypeface(App.getInstance().getTypeface());

        if(this.getTypeface()!=null){
            if(this.getTypeface().getStyle()== Typeface.BOLD){
                setTypeface(App.getInstance().getTypeface("AvenirBold"));
            }
        }

    }
}
